//
//  Person.swift
//  BMI Calculator
//
//  Created by iosdev on 5.11.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import UIKit

class Person {
    
    //MARK: Properties
    
    var persons = [Person]()
    var name: String
    private var height: Int
    private var weight: Int
    var weightIndex: Double?
    
    //MARK: Initilization
    
    init?(name: String, height: Int, weight: Int){
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        self.name = name
        self.height = height
        self.weight = weight
        bMICalc()
    }
    
    func bMICalc() {
        if name != "" {
            let heightInMeters = (Double(self.height)/100)
            let unRoundedWeightIndex = (Double(self.weight) / (pow(heightInMeters, 2)))
            weightIndex = (unRoundedWeightIndex*100).rounded()/100
        }
    }
    
}
