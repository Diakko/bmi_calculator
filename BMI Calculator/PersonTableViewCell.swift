//
//  PersonTableViewCell.swift
//  BMI Calculator
//
//  Created by iosdev on 8.11.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    // MARK: Properties
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
