//
//  ViewController.swift
//  BMI Calculator
//
//  Created by iosdev on 4.11.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate  {
    
      
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var pickerOptions = [[String]]()
    var pickerWeightData = [String]()
    var pickerWeightOptions = [String]()
    var pickerHeightData = [String]()
    var pickerHeightOptions = [String]()
    var weightSelected: Int = 140
    var heightSelected: Int = 40
    var persons = [Person]()
    var person: Person?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        insertIntoArrays()
        loadSamplePersons()
        
        nameField.delegate = self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: UIPickerViewDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerOptions[component].count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerOptions[component][row]
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
        heightSelected = Int(pickerHeightData[pickerView.selectedRow(inComponent: 0)]) ?? 140
        weightSelected = Int(pickerWeightData[pickerView.selectedRow(inComponent: 2)]) ?? 40
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField,
      shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ").inverted
      return (string.rangeOfCharacter(from: invalidCharacters) == nil)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            return true
        } else {
            let alert = UIAlertController(title: "Error", message: "Enter your name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            return false
        }
    }
    
    // MARK: UITableViewDelegate

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
        // Fetch data for the cell
        let person = persons[indexPath.row]
        cell.textLabel?.text = "\(person.name): BMI: \(person.weightIndex ?? 0.0)"
        
        return cell
    }
    
    
    // MARK: Actions
    
    @IBAction func calculateBMI(_ sender: UIButton) {
        let name = nameField.text ?? ""
        
        guard let person = Person(name: name, height: heightSelected, weight: weightSelected) else {
            fatalError("Unable to instantiate person")
        }
        persons.append(person)
        tableView.reloadData()
        resultLabel.text = "Your BMI is \(person.weightIndex ?? 0.0)"
        
    }
    
    func insertIntoArrays() {
        
        for i in 140...230 {
            pickerHeightData.append("\(i)")
        }
        pickerHeightOptions.append("cm")
        for i in 40...220 {
            pickerWeightData.append("\(i)")
        }
        pickerWeightOptions.append("Kg")
        
        
        pickerOptions.append(pickerHeightData)
        pickerOptions.append(pickerHeightOptions)
        pickerOptions.append(pickerWeightData)
        pickerOptions.append(pickerWeightOptions)
    }
    
    func loadSamplePersons() {
        guard let person1 = Person(name: "Janne", height: 180, weight: 110) else {
            fatalError("Unable to instantiate person1")
        }
        
        guard let person2 = Person(name: "Matias", height: 188, weight: 88) else {
            fatalError("Unable to instantiate person2")
        }
        
        guard let person3 = Person(name: "Tiina", height: 1650, weight: 50) else {
            fatalError("Unable to instantiate person3")
        }
    
        persons += [person1, person2, person3]
    
    }
    
}

